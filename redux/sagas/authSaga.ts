import { call, put, takeEvery } from 'redux-saga/effects'
import {DO_AUTH, DO_AUTH_FAIL, DO_AUTH_SUCCESS} from "../constants/authConstants";
import {DoAuth} from '../actions/authActions';
import { apiPost } from '../../api/api';
import AsyncStorage from '@react-native-async-storage/async-storage';


function* doAuth (action : DoAuth){
    try {
        const result = yield call(apiPost, '/login', {PHONE: action.payload?.LOGIN, PASSWORD: action.payload?.PASSWORD});
        if(result?.result?.accessToken && result?.result?.refreshToken) {
            yield AsyncStorage.setItem('accessToken', result?.result?.accessToken);
            yield AsyncStorage.setItem('refreshToken', result?.result?.refreshToken);
            return yield put({type: DO_AUTH_SUCCESS});
        }
        if(result.error) {
            const errorPayload = result.error?.message ? result.error : {message: 'Неизвестная ошибка'};
            return yield put({type: DO_AUTH_FAIL, payload: errorPayload});
        }
        return yield put({type: DO_AUTH_FAIL, payload: {message: 'Неизвестная ошибка'}});
    } catch (e) {
        yield put({type: DO_AUTH_FAIL, payload: {message: 'Сервис временно недоступен'}});
    }
}

export function* watchDoAuth() {
    yield takeEvery(DO_AUTH, doAuth);
}