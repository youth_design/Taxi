import { call, put, takeLatest } from 'redux-saga/effects'
import {FetchHints} from "../actions/routeHintsActions";
import {FETCH_HINTS, FETCH_HINTS_ERROR, FETCH_HINTS_SUCCESS} from "../constants/routeHintsConstants";
import {getAddressesByStringQueryYandex} from "../../api/mapApi";
import {RouteHint} from "../../types/routeHints";
import {Cords} from "../../types/map";
import {parseMapsGeoObjectYandex} from "../../shared/utils";

function* fetchHints(action: FetchHints) {
    try {
        const query = action.payload;
        const response = yield call(getAddressesByStringQueryYandex, query);
        const hints = parseMapsGeoObjectYandex(response);
        if(hints?.length) {
            yield put({type: FETCH_HINTS_SUCCESS, payload: hints});
        } else {
            yield put({type: FETCH_HINTS_SUCCESS, payload: []})
        }
    } catch (e) {
        console.error(e);
        yield put({type: FETCH_HINTS_ERROR})
    }
}

export function* watchFetchHints() {
    yield takeLatest(FETCH_HINTS, fetchHints);
}