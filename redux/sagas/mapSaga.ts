import { call, put, takeLatest } from 'redux-saga/effects'
import {SetCurrentPosition, SetStartAddress} from "../actions/mapActions";
import {getAddressByRegionYandex, getCurrentGEO} from "../../api/mapApi";
import {
    SET_CURRENT_POSITION, SET_CURRENT_POSITION_ERROR, SET_CURRENT_POSITION_SUCCESS,
    SET_START_ADDRESS,
    SET_START_ADDRESS_ERROR,
    SET_START_ADDRESS_SUCCESS
} from "../constants/mapConstants";
import {parseMapsGeoObjectYandex, validateCords} from "../../shared/utils";

function* fetchAddressByRegion(action: SetStartAddress) {
    try {
        const region = action?.payload;
        const response = yield call(getAddressByRegionYandex, region);
        console.log('response:',response)
        const parsed = parseMapsGeoObjectYandex(response);
        if(parsed?.length) {
            yield put({
                type: SET_START_ADDRESS_SUCCESS,
                payload: {
                    address: parsed[0].title,
                    region: {
                        lat: parsed[0].lat,
                        long: parsed[0].long
                    }
                }
            })
        }

        // const components = parseMapsGeoObject(response);
        //
        // let formattedAddress = [components.locality ? (components.locality) : ''];
        // formattedAddress = [...formattedAddress, components.route ? (components.route) : ''];
        // formattedAddress = [...formattedAddress, components.street_number ? (components.street_number) : ''];
        //
        // yield put({type: SET_START_ADDRESS_SUCCESS, payload: {address: formattedAddress.join(', '), region}})
    } catch (e) {
        yield put({type: SET_START_ADDRESS_ERROR})
    }
}

function* fetchCurrentPosition (action: SetCurrentPosition) {
    try {
        const response = yield call(getCurrentGEO);
        validateCords(response)
        yield put({
            type: SET_CURRENT_POSITION_SUCCESS,
            payload: {
                currentPosition: {
                    lat: response.lat,
                    long: response.long
                }
            }
        })

    } catch (e) {
        yield put({type: SET_CURRENT_POSITION_ERROR})
    }

}

function* watchFetchAddressByRegion() {
    yield takeLatest(SET_START_ADDRESS, fetchAddressByRegion)
    yield takeLatest(SET_CURRENT_POSITION, fetchCurrentPosition)
}

export default watchFetchAddressByRegion;