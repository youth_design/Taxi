import { call, put, takeEvery } from 'redux-saga/effects';
import { apiPost } from '../../api/api';
import {RegistrationRequest} from '../actions/registrationsActions';
import { REGISTRATION_ERROR, REGISTRATION_REQUEST, REGISTRATION_SUCCESS } from '../constants/registrationConstants';

function* registrationRequest(action: RegistrationRequest) {
  try {
    const result = yield call(apiPost, '/register', {
      ...action.payload,
    });
    if(result?.error) {
      return yield put({type: REGISTRATION_ERROR, payload: result.error});
    }
    return yield put({type: REGISTRATION_SUCCESS, payload: result.result});
  } catch(err) {
    console.log(err);
    return yield put({type: REGISTRATION_ERROR, payload: {message: 'Неизвестная ошибка'}});
  }
}

export function* watchRegistrationRequest() {
  yield takeEvery(REGISTRATION_REQUEST, registrationRequest);
}