import { all } from 'redux-saga/effects';
import {watchDoAuth} from "./authSaga";
import watchFetchAddressByRegion from "./mapSaga";
import {watchFetchHints} from "./routeHintsSaga";
import { watchRegistrationRequest } from './registrationSaga';

export default function* rootSaga() {
    yield all([
        watchDoAuth(),
        watchFetchAddressByRegion(),
        watchFetchHints(),
        watchRegistrationRequest(),
    ]);
}