import {RouteHint} from "../../types/routeHints";
import {RouteHintsTypes} from "../actions/routeHintsActions";

type RouteHintsState = {
    isFetching: Boolean,
    isError: Boolean,
    hints: Array<RouteHint>,
}

const initialState: RouteHintsState = {
    isFetching: false,
    isError: false,
    hints: [],
}

const handInputHintsReducer = function (state: RouteHintsState = initialState, action: RouteHintsTypes): RouteHintsState {
    switch(action.type) {
        case "FETCH_HINTS":
            return {
                ...state,
                isFetching: true,
                isError: false,
                hints: [],
            };
        case "FETCH_HINTS_SUCCESS":
            return {
                ...state,
                isFetching: false,
                isError: false,
                hints: action.payload || [],
            };
        case "FETCH_HINTS_ERROR":
            return {
                ...state,
                isFetching: false,
                isError: true,
                hints: []
            }
        default:
            return state;
    }
}

export default handInputHintsReducer;