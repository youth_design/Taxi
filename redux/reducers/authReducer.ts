import {DoAuthActionTypes} from "../actions/authActions";
import {DO_AUTH, DO_AUTH_FAIL, DO_AUTH_SUCCESS} from "../constants/authConstants";

export type AuthState = {
    isAuth: boolean,
    isError: boolean,
    isFetching: boolean,
    errorMessage: string,
}

const initialState: AuthState = {
    isAuth: false,
    isError: false,
    isFetching: false,
    errorMessage: '',
}

const authReducer = function(state: AuthState = initialState, action : DoAuthActionTypes) : AuthState {
    switch(action.type) {
        case DO_AUTH:
            return {
                ...initialState,
                isFetching: true,
            }
        case DO_AUTH_SUCCESS:
            return {
                ...initialState,
                isAuth: true,
            }
        case DO_AUTH_FAIL:
            return {
                ...initialState,
                isError: true,
                errorMessage: action.payload?.message || '',
            }
        default:
            return state;
    }
}

export default authReducer;