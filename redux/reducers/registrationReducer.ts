import { RegistrationResponse } from "../../types";
import {RegistrationActionTypes} from "../actions/registrationsActions";
import {
    REGISTRATION_ERROR,
    REGISTRATION_REQUEST,
    REGISTRATION_RESET_STATE,
    REGISTRATION_SUCCESS
} from "../constants/registrationConstants";

type RegistrationState = {
    isFetching: boolean;
    isSuccess: boolean;
    isError: boolean;
    newUserData: RegistrationResponse | null;
    errorMessage: string | null;
};

const initialState: RegistrationState = {
    isFetching: false,
    isSuccess: false,
    isError: false,
    newUserData: null,
    errorMessage: null,
};

const registrationReducer = (state: RegistrationState = initialState, action: RegistrationActionTypes): RegistrationState => {
    switch(action.type) {
        case REGISTRATION_REQUEST:
            return {
                ...initialState,
                isFetching: true,
            };
        case REGISTRATION_SUCCESS:
            return {
                ...initialState,
                isSuccess: true,
                newUserData: action.payload || null,
            };
        case REGISTRATION_ERROR: 
            return {
                ...initialState,
                isError: true,
                errorMessage: action.payload?.message || null,
            };
        case REGISTRATION_RESET_STATE:
            return initialState;
        default: {
            return state;
        }
    }
}

export default registrationReducer;
