import {Cords} from "../../types/map";
import {MapActionTypes} from "../actions/mapActions";
import {
    SET_CURRENT_POSITION, SET_CURRENT_POSITION_ERROR, SET_CURRENT_POSITION_SUCCESS,
    SET_START_ADDRESS,
    SET_START_ADDRESS_ERROR,
    SET_START_ADDRESS_SUCCESS
} from "../constants/mapConstants";

type MapState = {
    isFetching: boolean,
    isError: boolean,
    addressStart: string,
    addressEnd: string,
    regionStart: Cords,
    regionEnd:  Cords,
    currentPosition: Cords
}

const initialState = {
    isFetching: false,
    isError: false,
    addressStart: '',
    addressEnd: '',
    regionStart: null,
    regionEnd:  null,
    currentPosition: null
}

const mapReducer = (state: MapState = initialState, action: MapActionTypes) : MapState => {
    switch (action.type) {
        case SET_START_ADDRESS:
            return {
                ...state,
                isFetching: true,
                isError: false,
                regionStart: null,
            }
        case SET_START_ADDRESS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isError: false,
                addressStart: action.payload?.address || '',
                regionStart: action.payload?.region || null,
            }
        case SET_START_ADDRESS_ERROR:
            return {
                ...state,
                isFetching: false,
                isError: true,
                addressStart: '',
                regionStart: null
            }
        case SET_CURRENT_POSITION:
            return {
                ...state,
                isFetching: true,
                isError: false,
            }
        case SET_CURRENT_POSITION_ERROR:
            return {
                ...state,
                isFetching: false,
                isError: true,
            }
        case SET_CURRENT_POSITION_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isError: false,
                currentPosition: action.payload?.currentPosition
            }
        default:
            return state;
    }
}

export default mapReducer;