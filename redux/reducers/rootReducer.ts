import {combineReducers} from "redux";
import authReducer from "./authReducer";
import mapReducer from "./mapReducer";
import handInputHintsReducer from "./handInputHintsReducer";
import registrationReducer from "./registrationReducer";

const rootReducer =  combineReducers({
    auth: authReducer,
    map: mapReducer,
    routeHints: handInputHintsReducer,
    registration: registrationReducer,
});

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer;