import {RegistrationFields, RegistrationResponse} from "../../types";
import {
    REGISTRATION_ERROR,
    REGISTRATION_REQUEST,
    REGISTRATION_RESET_STATE,
    REGISTRATION_SUCCESS
} from "../constants/registrationConstants";
import {ErrorFromApi} from "../../types/types";
import {ActionType} from "../../types/actions";


export type RegistrationRequest = ActionType<typeof REGISTRATION_REQUEST, RegistrationFields>;
type RegistrationSuccess = ActionType<typeof REGISTRATION_SUCCESS, RegistrationResponse>;
type RegistrationError = ActionType<typeof REGISTRATION_ERROR, ErrorFromApi>;
type RegistrationResetState = ActionType<typeof REGISTRATION_RESET_STATE, undefined>;

export type RegistrationActionTypes = RegistrationRequest | RegistrationSuccess | RegistrationError | RegistrationResetState;

export const registrationRequest = (registrationData: RegistrationFields): RegistrationRequest => {
    return {
        type: REGISTRATION_REQUEST,
        payload: registrationData,
    };
};

export const registrationSuccess = (successData: RegistrationResponse): RegistrationSuccess => {
    return {
        type: REGISTRATION_SUCCESS,
        payload: successData,
    };
};

export const registrationError = (error: ErrorFromApi): RegistrationError => {
    return {
        type: REGISTRATION_ERROR,
        payload: error
    };
};

export const registrationResetState = (): RegistrationResetState => {
    return {
        type: REGISTRATION_RESET_STATE,
    }
};