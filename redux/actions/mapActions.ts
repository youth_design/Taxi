import {ActionType} from "../../types/actions";
import {Cords, FullAddressInfo} from "../../types/map";
import {
    SET_CURRENT_POSITION, SET_CURRENT_POSITION_ERROR, SET_CURRENT_POSITION_SUCCESS,
    SET_START_ADDRESS,
    SET_START_ADDRESS_ERROR,
    SET_START_ADDRESS_SUCCESS
} from "../constants/mapConstants";

export type SetStartAddress = ActionType<typeof SET_START_ADDRESS, Cords>;
type SetStartAddressSuccess = ActionType<typeof SET_START_ADDRESS_SUCCESS, FullAddressInfo>;
type SetStartAddressError = ActionType<typeof SET_START_ADDRESS_ERROR, undefined>;
export type  SetCurrentPosition = ActionType<typeof SET_CURRENT_POSITION, undefined>;
export type  SetCurrentPositionError = ActionType<typeof SET_CURRENT_POSITION_ERROR, undefined>;
export type  SetCurrentPositionSuccess = ActionType<typeof SET_CURRENT_POSITION_SUCCESS, Cords>;

export type MapActionTypes = SetStartAddress | SetStartAddressSuccess | SetStartAddressError | SetCurrentPosition | SetCurrentPositionError | SetCurrentPositionSuccess;

export const setStartAddress = (region: Cords): SetStartAddress => {
    return {
        type: SET_START_ADDRESS,
        payload: region,
    }
}

export const setStartAddressSuccess = (fullAddressInfo: FullAddressInfo): SetStartAddressSuccess => {
    return {
        type: SET_START_ADDRESS_SUCCESS,
        payload: fullAddressInfo,
    }
}

export const setStartAddressError = (): SetStartAddressError => {
    return {
        type: SET_START_ADDRESS_ERROR,
    }
}

export const setCurrentPosition = (): SetCurrentPosition => {
    return {
        type: SET_CURRENT_POSITION
    }
}

export const setCurrentPositionError = (): SetCurrentPositionError => {
    return {
        type: SET_CURRENT_POSITION_ERROR
    }
}
export const setCurrentPositionSuccess = (cords): SetCurrentPositionSuccess => {
    return {
        type: SET_CURRENT_POSITION_SUCCESS,
        payload: cords
    }
}