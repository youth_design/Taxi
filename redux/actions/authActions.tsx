import {DO_AUTH, DO_AUTH_FAIL, DO_AUTH_SUCCESS} from "../constants/authConstants";
import {ActionType} from "../../types/actions";
import { ErrorFromApi } from "../../types/types";
import {Credentials} from "../../types";

export type DoAuth = ActionType<typeof DO_AUTH, Credentials>;
type DoAuthSuccess = ActionType<typeof DO_AUTH_SUCCESS, undefined>;
type DoAuthFail = ActionType<typeof DO_AUTH_FAIL, ErrorFromApi>;

export type DoAuthActionTypes = DoAuth | DoAuthSuccess | DoAuthFail;

export const doAuth = (credentials: Credentials) : DoAuth => {
    return {
        type: DO_AUTH,
        payload: credentials
    };
};

export const doAuthSuccess = () : DoAuthSuccess => {
    return {
        type: DO_AUTH_SUCCESS
    };
};

export const doAuthFail = (error: ErrorFromApi) : DoAuthFail => {
    return {
        type: DO_AUTH_FAIL,
        payload: error,
    };
};