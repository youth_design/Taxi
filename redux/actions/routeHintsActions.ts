import {FETCH_HINTS, FETCH_HINTS_ERROR, FETCH_HINTS_SUCCESS} from "../constants/routeHintsConstants";
import {ActionType} from "../../types/actions";
import {RouteHint} from "../../types/routeHints";

export type FetchHints = ActionType<typeof FETCH_HINTS, string>;
type FetchHintsSuccess = ActionType<typeof FETCH_HINTS_SUCCESS, Array<RouteHint>>;
type FetchHintsError = ActionType<typeof FETCH_HINTS_ERROR, undefined>;

export type RouteHintsTypes = FetchHints | FetchHintsSuccess | FetchHintsError;

export const fetchHints = (query : string): FetchHints => {
    return {
        type: FETCH_HINTS,
        payload: query
    }
}

export const fetchHintsSuccess = (routeHints: Array<RouteHint>): FetchHintsSuccess => {
    return {
        type: FETCH_HINTS_SUCCESS,
        payload: routeHints,
    }
};

export const fetchHintsError = (): FetchHintsError => {
    return {
        type: FETCH_HINTS_ERROR,
    }
}