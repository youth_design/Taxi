import AsyncStorage from '@react-native-async-storage/async-storage';
import NetInfo from "@react-native-community/netinfo";

const apiUrl = 'http://192.168.0.103:4000/api'

const beforeRequest = async () => {
  const netState = await NetInfo.fetch();

  if(!netState.isConnected) {
    throw new Error("Ошибка подключения к интернету");
  }
}

const parseResponse = async (response: Response): Promise<any> => {
  const res = await response.json();
  const res_json = JSON.parse(res);

  if(res_json.error && res_json.error.code === 'BAD_ACCESS_TOKEN') {
    await updateAccessToken();
  }

  return res_json;
}

const apiPost = async (handler: string = '', body?: any, ...opts: any) : Promise<any> => {
  try {
    await beforeRequest();

    const accessToken = await AsyncStorage.getItem('accessToken');
    const response = await fetch(`${apiUrl}${handler}`, {
      ...opts,
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
        'authorization': accessToken ? `Bearer ${accessToken}` : '',
      }
    });

    const res_json = await parseResponse(response);

    return res_json;
  } catch(err) {
    throw new Error(err);
  }
}

const apiGet = async (handler: string, ...opts: any) : Promise<any> => {
    try {
      await beforeRequest();

      const accessToken = await AsyncStorage.getItem('accessToken');
      const response = await fetch(`${apiUrl}${handler}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'authorization': accessToken ? `Bearer ${accessToken}` : '',
        }
      });

      const res_json = await parseResponse(response);

      return res_json;
    } catch (err) {
      throw new Error(err);
    }
}

const updateAccessToken = async () => {
  const refreshToken = await AsyncStorage.getItem('refreshToken');
  const tokens = await apiPost('/token', {refreshToken: refreshToken});
}

export {
  apiPost,
  apiGet,
};
