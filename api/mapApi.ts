import {GOOGLE_MAPS_APIKEY, YANDEX_MAPS_APIKEY} from "../shared/constants";
import {Cords} from "../types/map";
import * as Location from "expo-location";

export const getAddressByRegion = (region: Cords | undefined): Promise<any> => {
    return fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${region?.lat},${region?.long}&language=ru&key=${GOOGLE_MAPS_APIKEY}`)
            .then(res => res.json())
}

export const getAddressesByStringQuery = (query: string | undefined): Promise<any> => {
    return fetch(`https://maps.googleapis.com/maps/api/place/nearbysearch/json?input=${query}&location=48.564814,39.308795&radius=100000&inputtype=textquery&fields=formatted_address,name&key=${GOOGLE_MAPS_APIKEY}`)
        .then(res => res.json())
}

export const getAddressByRegionYandex = (region: Cords | undefined): Promise<any> => {
    return fetch(`https://geocode-maps.yandex.ru/1.x?geocode=${region?.long},${region?.lat}&apikey=${YANDEX_MAPS_APIKEY}&format=json`)
        .then(res => res.json())
}

export const getAddressesByStringQueryYandex = (query: string | undefined): Promise<any> => {
    return fetch(`https://geocode-maps.yandex.ru/1.x?geocode=Луганская область ${query}&apikey=${YANDEX_MAPS_APIKEY}&format=json`)
        .then(res => res.json())
}

export const getCurrentGEO = async () => {
    let { status } = await Location.requestPermissionsAsync();
    if (status !== 'granted') {
        throw Error('error: no permission for geo')
    }
    try {
        const location = await Location.getCurrentPositionAsync({});
        return {lat: location.coords.latitude, long: location.coords.longitude}
    } catch (e) {
        throw Error(e)
    }
}