export const colors = {
    background: '#333533',
    text: '#ffffff',
    error: '#b02a25',
    plashqueBG: 'rgba(176, 42, 37, 0.8)',
}