import {RouteHint} from "../types/routeHints";
import {Cords} from "../types/map";
import {put} from "redux-saga/effects";
import {FETCH_HINTS_SUCCESS} from "../redux/constants/routeHintsConstants";

export const parseMapsGeoObject = (rawObj: any) => {
    const rawComponents = rawObj.results[0].address_components;
    const components: any = {};
    rawComponents.forEach((rawComponent: any) => {
        if(typeof rawComponent?.types.forEach === 'function') {
            rawComponent.types.forEach((type: any) => {
                if(typeof type === 'string') {
                    components[type] = rawComponent.long_name || '';
                }
            })
        }
    })
    return components;
}

export const parseMapsGeoObjectYandex = (response: any) => {
    const hints: Array<RouteHint> = [];
    const featureMember = response?.response?.GeoObjectCollection?.featureMember;
    if(featureMember?.length) {
        featureMember.forEach((item: any) => {
            let hintTitle = '';
            let cords: Cords = {lat: 0, long: 0};

            let rawCords = item?.GeoObject?.Point?.pos;
            if(rawCords) {
                rawCords = rawCords.split(' ');
                if(rawCords.length === 2) {
                    cords.lat = parseFloat(rawCords[1]);
                    cords.long = parseFloat(rawCords[0]);
                }
            }

            const addressComponents = item?.GeoObject?.metaDataProperty?.GeocoderMetaData?.Address?.Components;
            if(addressComponents) {
                const allowedKinds = ['locality', 'district', 'street',  'house'];
                const filteredAddressComponents = addressComponents.filter((component: any) => allowedKinds.indexOf(component?.kind) !== -1);
                hintTitle = filteredAddressComponents.map((item: any) => item.name).join(', ');
            }
            hints.push({...cords, title: hintTitle});
        })
        return hints;
    }

}

export const validateCords = ( coords: Cords) => {
    if(!coords.lat || !coords.long) {
        throw new Error('error validate cords')
    }
}

export function debounce(callback, wait) {
    let timeout;
    return (...args) => {
        const context = this;
        clearTimeout(timeout);
        timeout = setTimeout(() => callback.apply(context, args), wait);
    };
}
