import React from 'react';
import { DataContext } from "../dataProvider/DataProvider";

const withDataProvider = (Component: any) => (props: any) => {
    return (
        <DataContext.Consumer>
            {context => <Component {...props} {...context} />}
        </DataContext.Consumer>
    );
};

export default withDataProvider;