import React, {createContext, useEffect, useState} from 'react';
import {apiGet} from "../../../api/api";
import {withPlashque} from "../withPlashque/withPlashque";
import {TPlashqueContext} from "../plashqueProvider/PlashqueProvider";

type DataProviderProps = TPlashqueContext & {
    children: JSX.Element;
};

type Refs = {
    REACT_TEST_REF?: string;
};

export type TDataContext = {
    refs?: Refs;
};

export const DataContext = createContext<TDataContext>({});

const DataProvider = (props: DataProviderProps) => {
    const [refs, setRefs] = useState({});

    const fetchRefs = async () => {
        try {
            const _refs = await apiGet('/refs');
            if(_refs.result) {
                setRefs(_refs.result);
            }
        } catch(err) {
            throw new Error(err);
        }
    }

    useEffect(() => {
        fetchRefs();
    }, []);

    return (
        <DataContext.Provider value={{refs}}>
            {props.children}
        </DataContext.Provider>
    );
};

export default withPlashque(DataProvider);