import React from 'react'
import { PlashqueContext } from '../plashqueProvider/PlashqueProvider'

export const withPlashque = (Component: any) => (props: any) => {
  return (
    <PlashqueContext.Consumer>
      {(context) => <Component {...props} {...context} />}
    </PlashqueContext.Consumer>
  )
}
