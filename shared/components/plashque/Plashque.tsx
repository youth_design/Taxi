import React, {useEffect, useState} from 'react';
import {PlashqueParams} from "../plashqueProvider/PlashqueProvider";
import {Animated, StyleSheet, Text} from "react-native";
import {colors} from "../../../styles/colors";

type PlashqueProps = {
    params: PlashqueParams;
    hidePlashque: () => void;
}

const Plashque = (props: PlashqueProps) => {
    const position = useState(new Animated.Value(-20))[0];
    const opacity = useState(new Animated.Value(1))[0];

    useEffect(() => {
        Animated.timing(position, {
            duration: 300,
            toValue: 0,
            useNativeDriver: false,
        }).start();
        setTimeout(() => {
            Animated.timing(opacity, {
                duration: 300,
                toValue: 0,
                useNativeDriver: false,
            }).start();
        }, props.params.timeout - 300);
    }, []);

    return (
        <Animated.View style={{...styles.plashque, top: position, opacity: opacity }}>
            <Text style={styles.text}>{props.params.content}</Text>
            <Text style={styles.cross} onPress={props.hidePlashque}>x</Text>
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    plashque: {
        position: 'relative',
        zIndex: 100,
        backgroundColor: colors.plashqueBG,
        paddingLeft: 10,
        paddingRight: 20,
        paddingTop: 2,
        paddingBottom: 2,
        borderRadius: 3,
        height: 22,
        margin: 8
    },
    text: {
        color: colors.text,
        fontSize: 12,
    },
    cross: {
        fontSize: 12,
        fontWeight: '900',
        position: "absolute",
        color: colors.text,
        right: 10,
        top: 0,
        transform: [{scale: 1.5}]
    }
});


export default Plashque;