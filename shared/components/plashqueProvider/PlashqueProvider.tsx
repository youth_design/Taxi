import React, { createContext, useState } from 'react'
import {View, StyleSheet } from 'react-native'
import Plashque from "../plashque/Plashque";
type PlashqueProviderProps = {
  children?: JSX.Element;
};

export type PlashqueParams = {
  timeout: number;
  content: string;
  id?: string;
};

export type TPlashqueContext = {
  showPlashque?: (params: PlashqueParams) => void
}

export const PlashqueContext = createContext<TPlashqueContext>({}); 

export const PlashqueProvider = (props: PlashqueProviderProps) => {
  const [plashques, setPlashques] = useState<PlashqueParams[]>([]);
  
  
  const showPlashque = (params: PlashqueParams) : void => {
    const id = Math.random().toString();
    setPlashques(prevState => [...prevState, {...params, id}]);
    setTimeout(() => {
      hidePlashque(id);
    }, params.timeout);
  }

  const hidePlashque = (id: string | undefined) => {
    setPlashques(prevState => prevState.filter(i => i.id !== id));
  }

  return (
    <PlashqueContext.Provider value={{ showPlashque }}>
      <View>
        <View>
          {props.children}
        </View>
        <View style={styles.wrapper}>
          {plashques.map((plashque: PlashqueParams) => (
              <Plashque key={plashque.id} params={plashque} hidePlashque={() => hidePlashque(plashque.id)} />
          ))}
        </View>
      </View>
    </PlashqueContext.Provider>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    justifyContent: 'center',
    top: 30,
    width: '100%',
  }
});