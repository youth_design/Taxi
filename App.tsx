import React from 'react';
import { Provider } from 'react-redux';

import store from "./redux/store";

import Router from "./Router";
import { PlashqueProvider } from './shared/components/plashqueProvider/PlashqueProvider';

export default function App() {
    return (
        <Provider store={store}>
            <PlashqueProvider>
                <Router />
            </PlashqueProvider>
        </Provider>
        );
    }