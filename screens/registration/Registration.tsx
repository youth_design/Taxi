import React, {useEffect, useRef} from 'react';
import {StyleSheet, View} from "react-native";
import {Controller, useForm} from "react-hook-form";
import {RegistrationFields} from "../../types";
import {Button, Input} from "react-native-elements";
import {colors} from "../../styles/colors";
import { RootState } from '../../redux/reducers/rootReducer';
import { connect, ConnectedProps } from 'react-redux';
import { TPlashqueContext } from '../../shared/components/plashqueProvider/PlashqueProvider';
import { registrationRequest } from '../../redux/actions/registrationsActions';
import { withPlashque } from '../../shared/components/withPlashque/withPlashque';
import { RouteComponentProps } from 'react-router-native';

const validators = {
    PHONE: {
        required: 'Поле обязательно для заполнения',
    },
    EMAIL: {
        pattern: {
            value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            message: 'Неверный формат E-mail',
        }
    },
    NAME: {
        required: 'Поле обязательно для заполнения',
        pattern: {
            value: /^([а-яА-ЯЁё]+([. \-`']*|[. \-`']?([ivxlcIVXLC]?[.\-`']?))*?)?([ \-`']+[а-яА-ЯЁё]+[ \-`'.]*([ \-`'][ivxlcIVXLC]+)*)*$/,
            message: 'Неверный формат имени',
        }
    },
    PASSWORD: {
        required: 'Поле обязательно для заполнения',
        pattern: {
            value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
            message: 'Пароль должен состоять минимум из 8 символов, а также содержать 1 цифру и 1 букву',
        }
    },
    SUBMIT_PASSWORD: {
        required: 'Поле обязательно для заполнения',
    },
};

const mapStateToProps = (state: RootState) => ({
    registration: state.registration,
});

const mapDispatchToProps = {
    registrationRequest,
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type RegistrationProps = PropsFromRedux & TPlashqueContext & RouteComponentProps & {}

const Registration = (props: RegistrationProps) => {
    const { control, handleSubmit, errors, getValues } = useForm<RegistrationFields>();
    const emailField = useRef<Input | null>(null);
    const nameField = useRef<Input | null>(null);
    const passwordField = useRef<Input | null>(null);
    const submitPasswordField = useRef<Input | null>(null);

    useEffect(() => {
        if(props.registration.isError && props.showPlashque instanceof Function) {
            props?.showPlashque({content: props.registration.errorMessage || '', timeout: 4000});
        }
    }, [props.registration.isError]);

    useEffect(() => {
        if(props.registration.isSuccess) {
            props.history.push('/');
        }
    }, [props.registration.isSuccess])


    const onSubmitForm = (data: RegistrationFields) => {
        props.registrationRequest(data);
    }

    const validateSubmitPasswordDecorator = (getValues: any) => (value: string | undefined): string | boolean => {
        if(getValues("PASSWORD") === value) {
            return true;
        }
        return "Пароли не совпадают";
    }

    const putFocusOnEmail = () => {
        emailField.current?.focus();
    }

    const putFocusOnName = () => {
        nameField.current?.focus();
    }

    const putFocusOnPassword = () => {
        nameField.current?.focus();
    }

    const putFocusOnSubmitPassword = () => {
        submitPasswordField.current?.focus();
    }

    return (
        <View style={styles.wrapper}>
            <View style={styles.form}>
                <Controller
                    as={Input}
                    control={control}
                    name="PHONE"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators.PHONE}
                    defaultValue=""
                    placeholder="Номер телефона"
                    blurOnSubmit={false}
                    onSubmitEditing={putFocusOnEmail}
                    returnKeyType="go"
                    inputStyle={styles.input}
                    errorMessage={errors.PHONE
                        ? (errors.PHONE.message ? errors.PHONE.message.toString() : '')
                        : ''
                    }
                />
                <Controller
                    as={<Input ref={emailField} />}
                    control={control}
                    name="EMAIL"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators.EMAIL}
                    defaultValue=""
                    placeholder="E-mail"
                    blurOnSubmit={false}
                    onSubmitEditing={putFocusOnName}
                    returnKeyType="go"
                    autoCompleteType="email"
                    inputStyle={styles.input}
                    errorMessage={errors.EMAIL
                        ? (errors.EMAIL.message ? errors.EMAIL.message.toString() : '')
                        : ''
                    }
                />
                <Controller
                    as={<Input ref={nameField} />}
                    control={control}
                    name="NAME"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators.NAME}
                    defaultValue=""
                    placeholder="Имя"
                    blurOnSubmit={false}
                    onSubmitEditing={putFocusOnPassword}
                    returnKeyType="go"
                    autoCompleteType="name"
                    inputStyle={styles.input}
                    errorMessage={errors.NAME
                        ? (errors.NAME.message ? errors.NAME.message.toString() : '')
                        : ''
                    }
                />
                <Controller
                    as={<Input ref={passwordField} />}
                    control={control}
                    name="PASSWORD"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators.PASSWORD}
                    defaultValue=""
                    placeholder="Пароль"
                    blurOnSubmit={false}
                    onSubmitEditing={putFocusOnSubmitPassword}
                    returnKeyType="go"
                    autoCompleteType="password"
                    inputStyle={styles.input}
                    errorMessage={errors.PASSWORD
                        ? (errors.PASSWORD.message ? errors.PASSWORD.message.toString() : '')
                        : ''
                    }
                />
                <Controller
                    as={<Input ref={submitPasswordField} />}
                    control={control}
                    name="SUBMIT_PASSWORD"
                    onChange={args => args[0].nativeEvent.text}
                    rules={{...validators.SUBMIT_PASSWORD, validate: validateSubmitPasswordDecorator(getValues)}}
                    defaultValue=""
                    placeholder="Повторите пароль"
                    blurOnSubmit={false}
                    onSubmitEditing={handleSubmit(onSubmitForm)}
                    returnKeyType="go"
                    autoCompleteType="password"
                    inputStyle={styles.input}
                    errorMessage={errors.SUBMIT_PASSWORD
                        ? (errors.SUBMIT_PASSWORD.message ? errors.SUBMIT_PASSWORD.message.toString() : '')
                        : ''
                    }
                />
                <Button
                    type="solid"
                    loading={props.registration.isFetching}
                    disabled={props.registration.isFetching}
                    title="Зарегестрироваться"
                    onPress={handleSubmit(onSubmitForm)}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        backgroundColor: colors.background,
        padding: 70,
    },
    form: {
        justifyContent: 'center',
        width: '100%'
    },
    text: {
        color: colors.text,
        fontWeight: '700',
        fontSize: 20,
    },
    input: {
        color: colors.text,
    },
});

export default connector(withPlashque(Registration));