import React, {useEffect, useState} from 'react';
import MapRouteChoice from "./components/mapRouteChoice/MapRouteChoice";
import TextRouteChoice from "./components/textRouteChoice/TextRouteChoice";
import {View} from "react-native";
import {RootState} from "../../redux/reducers/rootReducer";
import {setStartAddress, setCurrentPosition} from "../../redux/actions/mapActions";
import {connect, ConnectedProps} from "react-redux";
import {Cords} from "../../types/map";


const mapStateToProps = (state: RootState) => ({
    map: state.map,
});

const mapDispatchToProps = {
    setStartAddress,
    setCurrentPosition
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type BookingProps = PropsFromRedux & {
    expanded: Boolean
}

const Booking = (props: BookingProps) => {
    const [expanded, setExpanded] = useState<Boolean>(false);

    return (
        <View>
            <MapRouteChoice
                expanded={expanded}
                setStartAddress={props.setStartAddress}
                setCurrentPosition={props.setCurrentPosition}
                regionStart={props.map.regionStart}
                regionEnd={props.map.regionEnd}
                currentPosition={props.map.currentPosition}
            />
            <TextRouteChoice
                expanded={expanded}
                setStartAddress={props.setStartAddress}
                setExpanded={setExpanded}
                addressStart={props.map.addressStart}
                addressEnd={props.map.addressEnd}
                mapIsFetching={props.map.isFetching}
            />
        </View>
    );
};

export default connector(Booking);
