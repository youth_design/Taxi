import React, {useEffect, useState, useRef, Dispatch, SetStateAction} from "react";
import {StyleSheet, View, ActivityIndicator, TextInput, Text, TouchableOpacity, Animated} from "react-native";
// @ts-ignore
import IconRoute from "../../../../assets/icon_route.svg";
// @ts-ignore
import MapIcon from "../../../../assets/icon_map.svg";
import useDebounce from "../../../../shared/debounce/useDebounce";
import {RootState} from "../../../../redux/reducers/rootReducer";
import {connect, ConnectedProps} from "react-redux";
import {fetchHints} from "../../../../redux/actions/routeHintsActions";
import {RouteHint} from "../../../../types/routeHints";
import {SetStartAddressAction} from "../../../../types/map";
import DismissKeyboard from "../../../common/DismissKeyboard";

const mapStateToProps = (state: RootState) => ({
    routeHints: state.routeHints,
});

const mapDispatchToProps = {
    fetchHints
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type TextRouteChoiceProps = PropsFromRedux & {
    expanded: Boolean;
    setExpanded: Dispatch<SetStateAction<Boolean>>;
    addressStart: string;
    addressEnd: string;
    setStartAddress: SetStartAddressAction;
    mapIsFetching: boolean;
}


const TextRouteChoice = (props: TextRouteChoiceProps) => {
    const expandedHeight = 450;
    const defaultHeight = 250;
    const slideTopAnim = useRef(new Animated.Value(expandedHeight)).current
    const [addressStart, setAddressStart] = useState('')
    const [addressEnd, setAddressEnd] = useState('')

    const debouncedQuery = useDebounce(addressStart, 500);

    useEffect(() => {
        if (debouncedQuery) {
            props.fetchHints(debouncedQuery);
        }
    }, [debouncedQuery])

    useEffect(() => {
        Animated.timing(slideTopAnim, {
            toValue: props.expanded ? expandedHeight : defaultHeight,
            duration: 300,
        }).start()
    }, [props.expanded])

    useEffect(() => {
        setAddressStart(props.addressStart);
    }, [props.addressStart])

    useEffect(() => {
        setAddressEnd(props.addressEnd)
    }, [props.addressEnd])

    return (
        <Animated.View style={[styles.container, {height: slideTopAnim}]}>
            <View style={styles.textFieldsContainer}>
                <View style={styles.routeIcon}>
                    <IconRoute/>
                </View>
                <View style={{width: '100%'}}>
                    <View>
                        <TextInput
                            onChangeText={value => setAddressStart(value)}
                            value={addressStart}
                            defaultValue=""
                            placeholder="Откуда"
                            blurOnSubmit={false}
                            style={[styles.input, styles.firstInput]}
                            returnKeyType="go"
                            // editable={props.expanded ? undefined : false}
                            onFocus={() => props.setExpanded(true)}
                            onBlur={() => props.setExpanded(false)}
                        />
                        {props.mapIsFetching &&
                        <View style={{position: 'absolute', top: 10, right: 30}}>
                            <ActivityIndicator size="large" color="#0000ff"/>
                        </View>
                        }
                    </View>
                    <TextInput
                        onChangeText={value => setAddressEnd(value)}
                        value={addressEnd}
                        defaultValue=""
                        placeholder="Куда"
                        blurOnSubmit={false}
                        style={styles.input}
                        returnKeyType="go"
                        // editable={props.expanded ? undefined : false}
                        onFocus={() => props.setExpanded(true)}
                        onBlur={() => props.setExpanded(false)}
                    />
                </View>
            </View>
            <TouchableOpacity style={styles.showOnMap} onPress={() => props.setExpanded(!props.expanded)}>
                <View>
                    <MapIcon/>
                </View>
                <Text style={styles.showOnMapText}>Показать на карте</Text>
            </TouchableOpacity>
            <View>
                {props.expanded &&
                (props.routeHints.isFetching
                        ? <ActivityIndicator size="large" color="#0000ff"/>
                        : props.routeHints.hints.map((hint: RouteHint) => (
                            <TouchableOpacity
                                key={hint.lat.toString() + hint.long.toString()}
                                onPress={() => props.setStartAddress({lat: hint.lat, long: hint.long})}
                            >
                                <Text>{hint.title}</Text>
                            </TouchableOpacity>)
                        )
                )}
            </View>
        </Animated.View>
    );
};

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        backgroundColor: '#ffffff',
        paddingHorizontal: 20,
        paddingTop: 25,
        paddingBottom: 10,
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
    },
    textFieldsContainer: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 1,
        elevation: 2,
        borderRadius: 10,
        backgroundColor: '#ffffff',
        display: 'flex',
        flexDirection: 'row',
        alignItems: "center",
        paddingLeft: 20,
        overflow: 'hidden'
    },
    input: {
        borderBottomWidth: 0,
        paddingVertical: 10,
        paddingHorizontal: 5,
        // marginLeft: 40,
    },
    firstInput: {
        borderBottomWidth: 1,
        borderBottomColor: '#D5DDE0'
    },
    routeIcon: {
        marginRight: 10,
    },
    showOnMap: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: "center",
        paddingLeft: 10
    },
    showOnMapText: {
        fontSize: 15,
        color: '#1152FD'
    }
})

export default connector(TextRouteChoice);
