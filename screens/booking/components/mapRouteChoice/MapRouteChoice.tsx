import React, {useEffect, useState, useRef} from 'react';
import {View, StyleSheet, Image, Button} from "react-native";
import MapView  from 'react-native-maps';
import useDebounce from "../../../../shared/debounce/useDebounce";
import {Cords, SetStartAddressAction} from "../../../../types/map";
import * as Location from 'expo-location';
import {SetCurrentPosition} from "../../../../redux/actions/mapActions";
import {debounce} from "../../../../shared/utils";

type RouteChoiceProps = {
    expanded: Boolean;
    setStartAddress: SetStartAddressAction;
    setCurrentPosition: SetCurrentPosition;
    regionStart: Cords;
    regionEnd: Cords;
    currentPosition: Cords;
}

const MapRouteChoice = (props: RouteChoiceProps) => {
    const [region, setRegion] = useState({
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.003,
        longitudeDelta: 0.003,
    });
    const ref = useRef(null)

    useEffect(() => {
        if (props.currentPosition && ref.current) {
            ref.current.animateToRegion({...region, latitude: props.currentPosition.lat, longitude: props.currentPosition.long}, 300)
        }
    }, [props.currentPosition]);

    const initialRegion = {
        latitude: 48.564814,
        longitude: 39.308795,
        latitudeDelta: 0.003,
        longitudeDelta: 0.003,
    }

    const debouncedRegion = useDebounce(region, 500);

    useEffect(() => {
        if(debouncedRegion && !props.expanded) {
            props.setStartAddress({lat: region.latitude, long: region.longitude});
        }
    }, [debouncedRegion])


    return (
        <View style={styles.container}>
            <View style={styles.mapContainer} pointerEvents={props.expanded ? 'none' : 'auto'}>
                <MapView
                    ref={ref}
                    style={styles.map}
                    showsUserLocation={true}
                    showsMyLocationButton={false}
                    onRegionChangeComplete={region => setRegion(region)}
                    initialRegion={initialRegion}
                >
                </MapView>
            </View>
            <Image source={require('../../../../assets/marker.png')} style={styles.markerImage}/>
            <View
                style={styles.btnGeoContainer}
            >
                <Button title='test' onPress={props.setCurrentPosition}/>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    btnGeoContainer: {
        position: 'absolute',
        top: '50%',
        alignSelf: 'flex-end'
    },
    mapContainer: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width: '100%',
        height: '100%',
    },
    markerImage: {
        zIndex: 100,
        position: 'absolute',
        alignSelf: 'center',
        top: '50%',
        left: '50%',
        width: 20,
        height: 20,
        transform: [
            {translateX: -10},
            {translateY: -20},
        ]
    }
})

export default MapRouteChoice;

