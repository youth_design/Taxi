import React from 'react';
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native';

const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback
        accessible={false}
        onPress={() => Keyboard.dismiss()}
    >
        <View>
            {children}
        </View>
    </TouchableWithoutFeedback>
);
export default DismissKeyboard