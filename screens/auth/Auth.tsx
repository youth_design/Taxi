import React, {useEffect, useRef} from 'react';

import {StyleSheet, Text, View} from 'react-native';
import {Button, Input} from "react-native-elements";
import Icon from 'react-native-vector-icons/FontAwesome';
import {useForm, Controller} from "react-hook-form";
import {connect, ConnectedProps} from "react-redux";

import {doAuth} from "../../redux/actions/authActions";

import {RootState} from "../../redux/reducers/rootReducer";

import {colors} from "../../styles/colors";
import { withPlashque } from '../../shared/components/withPlashque/withPlashque';
import { TPlashqueContext } from '../../shared/components/plashqueProvider/PlashqueProvider';
import { RouteComponentProps} from "react-router-native";
import {Credentials} from "../../types";
import {registrationResetState} from "../../redux/actions/registrationsActions";


const mapStateToProps = (state: RootState) => ({
    auth: state.auth,
    registration: state.registration,
});

const mapDispatchToProps = {
    doAuth,
    registrationResetState,
}

const connector = connect(mapStateToProps, mapDispatchToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type AuthProps = PropsFromRedux & TPlashqueContext & RouteComponentProps & {}

const Auth = (props: AuthProps) => {
    const { control, handleSubmit, errors } = useForm<Credentials>();
    const passwordField = useRef<Input>(null);

    const putFocusOnPasswordField = () => {
        if(passwordField) {
            passwordField.current?.focus();
        }
    }

    const onSubmitForm = (data: Credentials) => {
        props.doAuth(data);
    }

    const validators = {
        required: "Поле обязательно для заполнения",
        maxLength: {
            value: 40,
            message: 'Превышена максимальная длинна',
        },
    }

    useEffect(() => {
        if(props.auth.isError) {
            if(props.showPlashque instanceof Function && props.auth.errorMessage) {
                props.showPlashque({content: props.auth.errorMessage, timeout: 10000});
            }
        }
    }, [props.auth.isError]);

    const gotoRegistrationPage = () => {
        props.registrationResetState();
        props.history.push('/registration');
    }

    return (
        <View style={styles.wrapper}>
            <View style={styles.form}>
                <Text style={styles.text}>
                  Авторизация
                </Text>
                <Controller
                    as={Input}
                    control={control}
                    name="LOGIN"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators}
                    defaultValue={props.registration.newUserData?.PHONE}
                    placeholder="Номер телефона"
                    inputStyle={styles.input}
                    onSubmitEditing={putFocusOnPasswordField}
                    blurOnSubmit={false}
                    leftIcon={
                        <Icon
                            name='phone'
                            size={24}
                            color='black'
                            style={styles.text}
                        />
                    }
                    returnKeyType="go"
                    errorMessage={errors.LOGIN
                        ? (errors.LOGIN.message ? errors.LOGIN.message.toString() : '')
                        : props.auth.errorMessage
                    }
                />
                <Controller
                    as={<Input ref={passwordField} />}
                    control={control}
                    name="PASSWORD"
                    onChange={args => args[0].nativeEvent.text}
                    rules={validators}
                    defaultValue=""
                    blurOnSubmit={false}
                    leftIcon={
                        <Icon
                            name='lock'
                            size={24}
                            color='black'
                            style={styles.text}
                        />
                    }
                    returnKeyType="go"
                    autoCompleteType="password"
                    errorMessage={errors.PASSWORD
                        ? (errors.PASSWORD.message ? errors.PASSWORD.message.toString() : '')
                        : props.auth.errorMessage
                    }
                    placeholder="Пароль"
                    inputStyle={styles.input}
                    onSubmitEditing={handleSubmit(onSubmitForm)}
                />

                <Button
                    type="solid"
                    title="Войти"
                    loading={props.auth.isFetching}
                    disabled={props.auth.isFetching}
                    onPress={handleSubmit(onSubmitForm)}
                />
                <Text style={styles.regLink} onPress={gotoRegistrationPage}>Заргестрироваться</Text>

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%',
        backgroundColor: colors.background,
        padding: 70,
    },
    form: {
        justifyContent: 'center',
        width: '100%'
    },
    text: {
        color: colors.text,
        fontWeight: '700',
        fontSize: 20,
    },
    input: {
        color: colors.text,
        paddingLeft: 5
    },
    regLink: {
        color: colors.text,
        textDecorationLine: 'underline',
        marginTop: 10,
    }
});

export default connector(withPlashque(Auth));
