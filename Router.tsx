import React from 'react';
import { NativeRouter, Route, Switch } from "react-router-native";

import {connect, ConnectedProps} from "react-redux";
import {View} from "react-native";

import {RootState} from "./redux/reducers/rootReducer";

import Auth from "./screens/auth/Auth";
import Booking from "./screens/booking/Booking";
import DataProvider from "./shared/components/dataProvider/DataProvider";
import Registration from "./screens/registration/Registration";
import DismissKeyboard from "./screens/common/DismissKeyboard";

const mapStateToProps = (state: RootState) => ({
    auth: state.auth,
});
const connector = connect(mapStateToProps)

type PropsFromRedux = ConnectedProps<typeof connector>

type RouterProps = PropsFromRedux & {}


const Router = (props: RouterProps) => {
    return (
        <NativeRouter>
            <DismissKeyboard>
            <View>
                { props?.auth?.isAuth ?
                    <DataProvider>
                        <Switch>
                            <Route exact path="/" component={Booking} />
                        </Switch>
                    </DataProvider> :
                    <Switch>
                        <Route exact path="/" component={Auth}/>
                        <Route path="/registration" component={Registration} />
                    </Switch>
                }
            </View>
            </DismissKeyboard>
        </NativeRouter>
    );
};

export default connector(Router);
