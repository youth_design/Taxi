export type Credentials = {
    LOGIN: string,
    PASSWORD: string,
}

export type RegistrationFields = {
    PHONE: string;
    EMAIL: string;
    NAME: string;
    PASSWORD: string;
    SUBMIT_PASSWORD?: string;
}

export type RegistrationResponse = {
    NAME: string;
    PHONE: string
}