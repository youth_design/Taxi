import {Cords} from "./map";

export type RouteHint = Cords & {
    title: string
}