import {SetStartAddress} from "../redux/actions/mapActions";

export type Cords = {
    lat: number,
    long: number,
} | null;

export type FullAddressInfo = {
    region: Cords,
    address: string,
}

export type SetStartAddressAction = (cords: Cords) => SetStartAddress