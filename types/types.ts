export type ErrorFromApi = {
  message?: string,
  code?: string,
}